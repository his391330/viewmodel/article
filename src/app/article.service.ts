import { Article } from "./article";

export interface ArticleService {
  getArticles(): Promise<Article[]>;

  removeArticle(id: number): any;

  updateArticle(article: Article): any;
}
